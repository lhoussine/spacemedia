package org.wikimedia.commons.donvip.spacemedia.data.domain.webmil;

import java.time.LocalDateTime;

import org.wikimedia.commons.donvip.spacemedia.data.domain.Media;

public class WebMilMedia extends Media<String, LocalDateTime> {

    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setId(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public LocalDateTime getDate() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDate(LocalDateTime date) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAudio() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isImage() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isVideo() {
        // TODO Auto-generated method stub
        return false;
    }

    // https://www.web.dma.mil/Our-Customers/#space-force-websites

    // NRO
    // https://www.nro.gov/Media/Images/

    // USSF
    // https://www.spaceforce.mil/Multimedia/Photos/
    // https://www.buckley.spaceforce.mil/News/Photos/
    // https://www.losangeles.spaceforce.mil/News/Photos/
    // https://www.patrick.spaceforce.mil/News/Photos/
    // https://www.spacebasedelta1.spaceforce.mil/Newsroom/Photos/
    // https://www.schriever.spaceforce.mil/News/Photos/
    // https://www.vandenberg.spaceforce.mil/News/Photos/

    // SPOC
    // https://www.spoc.spaceforce.mil/Connect-With-Us/Photos

    // STARCOM
    // https://www.starcom.spaceforce.mil/Connect-With-Us/Photos/

    // AFSPC
    // https://www.afspc.af.mil/News/Photos/
}

package org.wikimedia.commons.donvip.spacemedia.data.domain.eso;

public enum EsoMediaType {
    Artwork,
    Chart,
    Collage,
    Observation,
    Photographic,
    Planetary,
    Simulation
}

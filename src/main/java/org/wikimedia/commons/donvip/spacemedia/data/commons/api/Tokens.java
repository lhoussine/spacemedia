package org.wikimedia.commons.donvip.spacemedia.data.commons.api;

public class Tokens {

    private String csrftoken;

    public String getCsrftoken() {
        return csrftoken;
    }

    public void setCsrftoken(String csrftoken) {
        this.csrftoken = csrftoken;
    }
}
